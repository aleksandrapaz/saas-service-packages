﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;
using SaaSService.Data;
using SaaSService.Models;
using SaaSService.Services;
using SaaSService.Utils;

namespace SaaSService.Controllers
{
    [ApiController]
    [Route("api")]
    public class ApiController : Controller
    {
        // we will use the default country 
        // if we are not able to retrieve it from user's IP address
        private const string defaultCountry = "UK";
        private readonly ILogger<ApiController> _logger;
        private readonly LocationService _locationService;
        private readonly DbService _dbService;
        private readonly IActionContextAccessor _accessor;

        public ApiController(IActionContextAccessor accessor, DbService dbService, LocationService locationService, ILogger<ApiController> logger)
        {
            _logger = logger;
            _locationService = locationService;
            _dbService = dbService;
            _accessor = accessor;
        }

        [HttpGet]
        [Route("packages")]
        public async Task<List<PackageResponse>> GetPackages(string country)
        
        {
            // For this example, we add query parameter country (because all requests will come from localhost
            // once we deploy this to production, we will rely on the _locationService
            // because locationService will retrieve all time localhost, we will set UK as default
            if (country == null)
            {
                var request = _accessor.ActionContext.HttpContext.Request;
                var ipAddress = IPUtils.FindIpFromRequest(request);
                country = await _locationService.FindLocationByIPAsync(ipAddress).ConfigureAwait(false);
            }
            if (country == null)
            {
                country = defaultCountry;
            }
            CountryDetailEntity countryDetailEntity = _dbService.FindCountryDetails(country);
            if (countryDetailEntity == null)
            {
                throw new Exception("Country not supported.");
            }
            List<PackageEntity> packageEntities = _dbService.FindPackagesForCountry(countryDetailEntity);

            return packageEntities.Select(pe =>
            {
                var package = new PackageResponse();
                package.Id = pe.Id;
                package.Currency = pe.Currency;
                package.PackageType = pe.PackageType.ToString();
                package.Name = pe.Name;
                package.Price = CalculatePrice(pe, countryDetailEntity);
                return package;
            }).ToList();
        }

        private double CalculatePrice(PackageEntity package, CountryDetailEntity country)
        {
            var price = package.Price;
            var vat = country.VAT; // number between [0, 100]
            return Math.Round(price * (100 + vat) / 100, 2);
        }
    }
}
