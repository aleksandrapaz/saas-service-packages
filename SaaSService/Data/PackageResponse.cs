﻿using SaaSService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaaSService.Data
{
    // this object is different from the PackageEntity,
    // and it's used when we want to return data to the API
    // with VAT calculations included in the Price
    public class PackageResponse
    {
        public long Id { set; get; }

        public string Name { set; get; }

        public double Price { set; get; }

        public string PackageType { set; get; }

        public string Currency { set; get; }
    }
}
