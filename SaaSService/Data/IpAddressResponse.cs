﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace SaaSService
{
    public class IpAddressResponse
    {
        public string country_code { get; set; }

        public string country_name { get; set; }
    }
}
