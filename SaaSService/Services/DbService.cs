﻿using SaaSService.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SaaSService.Services
{
    // Note: This is a dummy service used to simplify this solution.
    // In future, we will replace this one with entity framework and
    // we will store the data into the Database, instead of creating static lists here
    public class DbService
    {
        private List<CountryDetailEntity> countryDetailsList;
        private List<ZoneEntity> zonesList;
        private List<PackageEntity> packagesList;

        public DbService()
        {
            //fill with mock data
            zonesList = new List<ZoneEntity>();
            var zone1 = new ZoneEntity(1, "EuroZone"); //countries from European monetary union
            var zone2 = new ZoneEntity(2, "USAZone");
            var zone3 = new ZoneEntity(3, "StandardZone");
            zonesList.Add(zone1);
            zonesList.Add(zone2);
            zonesList.Add(zone3);

            countryDetailsList = new List<CountryDetailEntity>();
            countryDetailsList.Add(new CountryDetailEntity(1, "MK", zone3, 18));
            countryDetailsList.Add(new CountryDetailEntity(2, "UK", zone1, 15));
            countryDetailsList.Add(new CountryDetailEntity(3, "US", zone2, 0));
            countryDetailsList.Add(new CountryDetailEntity(4, "BG", zone1, 18));
            countryDetailsList.Add(new CountryDetailEntity(5, "SR", zone3, 18));

            packagesList = new List<PackageEntity>();

            packagesList.Add(new PackageEntity(1, "EU Monthly Package", 9, PackageType.Monthly, zone1, "EUR"));
            packagesList.Add(new PackageEntity(2, "EU Yearly Package", 80, PackageType.Yearly, zone1, "EUR"));

            packagesList.Add(new PackageEntity(3, "US Monthly Package", 12, PackageType.Monthly, zone2, "$"));
            packagesList.Add(new PackageEntity(4, "US Yearly Package", 95, PackageType.Yearly, zone2, "$"));

            //for the simplicity of the example, we will use EUR currency for standard packages
            packagesList.Add(new PackageEntity(5, "Standard Monthly Package", 10, PackageType.Monthly, zone3, "EUR"));
            packagesList.Add(new PackageEntity(6, "Standard Yearly Package", 100, PackageType.Yearly, zone3, "EUR"));

        }

        public List<PackageEntity> FindPackagesForCountry(CountryDetailEntity country)
        {
            return packagesList.Where(t => t.Zone == country.Zone).ToList();
        }

        public CountryDetailEntity FindCountryDetails(string countryName)
        {
            return countryDetailsList.First(t => t.CountryName.Equals(countryName));
        }
    }
}
