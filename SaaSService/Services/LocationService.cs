﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace SaaSService.Services
{
    // Using an external API service - ipapi.com to retrieve Country from IP address
    public class LocationService
    {
        private readonly IHttpClientFactory _clientFactory;

        public LocationService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<string> FindLocationByIPAsync(string ipAddress)
        {
            string uri = $"http://api.ipapi.com/{ipAddress}?access_key=85f16f3a979c1c117cf4ef5005bdd71a";

            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("User-Agent", "HttpClientFactory-Sample");

            var client = _clientFactory.CreateClient();
            var response = await client.SendAsync(request).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
                var ipAddressResponse = await JsonSerializer.DeserializeAsync
                    <IpAddressResponse>(responseStream).ConfigureAwait(false);

                return ipAddressResponse.country_code;
            }
            else
            {
                return null;
            }
        }
    }
}
