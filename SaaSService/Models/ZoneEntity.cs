﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaaSService.Models
{
    public class ZoneEntity : BaseEntity
    {
        public string Name { set; get; }

        public ZoneEntity(long id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
