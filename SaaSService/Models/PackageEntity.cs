﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaaSService.Models
{
    public class PackageEntity : BaseEntity
    {
        public string Name { set; get; }

        public double Price { set; get; }

        public PackageType PackageType { set; get; }

        public ZoneEntity Zone { set; get; }
        
        public string Currency { set; get; }

        public PackageEntity(long id, string name, double price, PackageType packageType, ZoneEntity zone, string currency)
        {
            Id = id;
            Name = name;
            Price = price;
            PackageType = packageType;
            Zone = zone;
            Currency = currency;
        }
    }

    public enum PackageType
    {
        Monthly = 1,
        Yearly = 2
    }
}
