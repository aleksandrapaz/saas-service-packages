﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaaSService.Models
{
    public class BaseEntity
    {
        public long Id { set; get; }
    }
}
