﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace SaaSService.Models
{
    public class CountryDetailEntity : BaseEntity
    {
        public string CountryName { set; get; }
        public ZoneEntity Zone { set; get; }
        public int VAT { set; get; }

        public CountryDetailEntity(long id, string countryName, ZoneEntity zone, int vAT)
        {
            Id = id;
            CountryName = countryName;
            Zone = zone;
            VAT = vAT;
        }
    }
}
