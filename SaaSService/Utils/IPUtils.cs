﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaaSService.Utils
{
    public class IPUtils
    {

        public static string FindIpFromRequest(HttpRequest request)
        {
            return request.HttpContext.Connection.RemoteIpAddress.ToString();
        }
    }
}
